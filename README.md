# Animated maps of global spread of Covid-19

## Cases

![](World_map_spread_cases_covid19.gif)

## Deaths

![](World_map_spread_deaths_covid19.gif)

## French version

![](Carte_Monde_covid19_CAS.gif)
![](Carte_Monde_covid19_DECES.gif)

## With time line

![](Carte_Monde_covid19_CAS_BIS.gif)